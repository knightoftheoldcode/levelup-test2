const { expect } = require('chai');
const request = require('superagent');

const app = require('../server/app.test.js');


describe('Level-Up - MEAN Template', () => {
  const port = 9000;
  const baseUrl = `http://localhost:${port}`;

  before((done) => {
    app.start(port, done);
  });

  after((done) => {
    app.stop(done);
  });

  describe('when requested at /', () => {
    it('should load', (done) => {
      request.get(baseUrl).end((err, res) => {
        expect(err).to.equal(null);
        expect(res).to.have.property('status', 200);
        expect(res.text).to.contain('Welcome to Express');
        done();
      });
    });
  });

  describe('when requested at /blahblahblah', () => {
    it('should not load', (done) => {
      request.get(`${baseUrl}/blahblahblah`).end((err, res) => {
        expect(err).to.not.equal(null);
        expect(res).to.have.property('status', 404);
        // expect(res.text).to.contain('Blah');
        done();
      });
    });
  });
});
