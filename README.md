[![Pipeline Status](https://gitlab.com/knightoftheoldcode/levelup-test2/badges/master/pipeline.svg)](https://gitlab.com/knightoftheoldcode/levelup-test2/commits/master) [![Coverage Report](https://gitlab.com/knightoftheoldcode/levelup-test2/badges/master/coverage.svg)](https://knightoftheoldcode.gitlab.io/levelup-test2/coverage/)


# What's MEAN CI/CD Template

MEAN CI/CD Template is the simplified Node.js / Express template used in the Level Up! CI/CD tutorial.
